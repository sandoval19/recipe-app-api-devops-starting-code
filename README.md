# DevOps with AWS Project using IaC

This project is build on Django REST Recipe App and usages:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - IAM
 - CloudWatch
 - EC2
 - S3
 - VPC
 - RDS
 - Terraform
 - Gitlab


## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000
